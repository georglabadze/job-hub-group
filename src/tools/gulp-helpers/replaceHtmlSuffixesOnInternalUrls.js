const through = require('through2');
module.exports = function () {
    return through.obj((vinylFile, encoding, callback) => {
        let transformedFile = vinylFile.clone();
        transformedFile.contents = Buffer.from(transformedFile.contents.toString().replace(/href="\/.+(\.html)\"/g, (match, selection) => {
            const replaced = match.replace('.html', '')
            const relPath = vinylFile.path.split('/src/html')[1]
            console.log(`Replace URL in ${relPath}: ${match} > ${replaced}`)
            return replaced
        }))
        return callback(null, transformedFile);
    });
};
