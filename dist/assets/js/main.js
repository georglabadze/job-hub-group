function setBasicCarousel (elem, param) { //Owl Carousel properties
	var owl = elem.owlCarousel({
		loop:               param.loop,
		margin:             param.margin,
		rewind:             param.rewind,
		nav:                param.nav,
		touchDrag:          param.touchDrag,
		dots:               param.dots,
		animateIn:          param.animateIn,
		animateOut:         param.animateOut,
		mouseDrag:          param.mouseDrag,
		autoplay:           param.autoplay,
		autoplaySpeed:      param.autoplaySpeed,
		autoplayTimeout:    param.autoplayTimeout,
		autoplayHoverPause: param.autoplayHoverPause,
		responsive:         param.responsive,
		onInitialized:      param.onInitialized,
		onTranslate:        param.onTranslate,
		onTranslated:       param.onTranslated,
		navContainer:       param.navContainer,
		dotsContainer:      param.dotsContainer,
	});
	if (param.lArrow) {
		param.lArrow.click(function (e) {
			e.preventDefault();
			owl.trigger('prev.owl.carousel', [300]);
		});
	}
	if (param.rArrow) {
		param.rArrow.click(function (e) {
			e.preventDefault();
			owl.trigger('next.owl.carousel');
		});
	}
	return owl;
}

function setCarousels () {
	//sliders without progressbar
	setBasicCarousel($('#testimonial-slider'), {
		dots:       true,
		loop:       true,
		margin:     30,
		responsive: {
			0:   {
				items: 1
			},
			767: {
				items: 2
			},
			991: {
				items: 3
			},
		}
	});
	setBasicCarousel($('#nonprofits-slider'), {
		dots:       true,
		loop:       true,
		margin:     20,
		responsive: {
			0:   {
				items: 1
			},
			767: {
				items: 2,
				margin: 10
			},
			991: {
				items: 3
			},
		}
	});
}

function tabLocation () { //redirect url to current tab
	let url = location.href.replace(/\/$/, "");

	if (location.hash) {
		const hash = url.split("#");
		$('#how-it-works a[href="#' + hash[1] + '"]').tab("show");
		url = location.href.replace(/\/#/, "#");
		history.replaceState(null, null, url);
		setTimeout(() => {
			$(window).scrollTop(0);
		}, 400);
	}

	$('a[data-toggle="tab"]').on("click", function () {
		let newUrl;
		const hash = $(this).attr("href");
		if (hash == "#consult" || hash == "#web-apps") {
			newUrl = url.split("#")[0];
		} else {
			newUrl = url.split("#")[0] + hash;
		}
		newUrl += " ";
		history.replaceState(null, null, newUrl);
	});
};

function hamburger () {
	$('.hamburger').click(function () {
		$(this).parent().parent().toggleClass('active');
		$('body, html').toggleClass('hidden');
	});
}

function footerDropdown () {
	if ($(window).width() <= 767) {
		$('.footer-list .mobile-dropdown').hide();
		$('.footer .f-title').not('.talk').click(function (e) {
			e.preventDefault();
			if ($(this).hasClass('active')) {
				$(this).removeClass('active');
				$(this).closest('.footer-list').find('.mobile-dropdown').stop().slideUp('fast');
			} else {
				$('.footer .f-title').not('.talk').removeClass('active');
				$(this).addClass('active');
				$('.footer-list .mobile-dropdown').stop().slideUp('fast');
				$(this).next('.mobile-dropdown').stop().slideToggle('fast');
				$(this).closest('.footer-list').siblings().find('.mobile-dropdown').stop().slideUp('fast');
			}
		});
	}
}

function accordeon () {
	$('.accordeon .ac-list .question').click(function (e) {
		e.preventDefault();
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$(this).closest('.ac-list').find('.answ-container').stop().slideUp('fast');
		} else {
			$('.accordeon .ac-list .question').removeClass('active');
			$(this).addClass('active');
			$(this).next('.answ-container').stop().slideToggle('fast');
			$(this).closest('.ac-list').siblings().find('.answ-container').stop().slideUp('fast');
		}
	});
}

function textareaLimit () {
	let maxchars = 300;
	$('textarea').keyup(function () {
		let tlength = $(this).val().length;
		let remain;
		$(this).val($(this).val().substring(0, maxchars));
		remain = maxchars - parseInt(tlength);
		if (remain == maxchars) {
			$('.char-counter').text('Max ' + remain);
		} else {
			$('.char-counter').text('Left ' + remain);
		}
	});
}

function getQuoteForm () {
	$('.btn-grn[data-next]').click(function (e) {
		e.preventDefault();
		$('.step').hide();
		var attr = $(this).attr('data-next');
		$("#" + attr + "").fadeIn('fast');
	})

	$('.back-btn[data-prev]').click(function (e) {
		e.preventDefault();
		$('.step').hide();
		var attr = $(this).attr('data-prev');
		$("#" + attr + "").fadeIn('fast');
	})
	$('.step-bullet[data-target]').click(function (e) {
		$('.step').hide();
		var attr = $(this).attr('data-target');
		$("#" + attr + "").fadeIn('fast');
	})
}

//replaces Bootstrap tabs on mobile screen size
function tabDropdown () {
	let url = location.href.replace(/\/$/, "");
	let placeholder = location.hash.substr(1).split('#');

	if (placeholder != '') {
		$('.tab-dropdown-label').text(placeholder);
	}

	$('.tab-dropdown-label').click(function (e) {
		e.stopPropagation();
		let selector = $(this).closest('.tab-dropdown');
		if (selector.hasClass('open')) {
			selector.removeClass('open');
		} else {
			$('.tab-dropdown-label').each(function (index, item) {
				$(item).closest('.tab-dropdown').removeClass('open');
			});
			selector.addClass('open');
		}
	});

	$('.tab-dropdown-list li').click(function () {
		let newUrl;
		let attr = $(this).attr('data-show');

		$(this).closest('.tab-dropdown').find('.tab-dropdown-label').text($(this).text());

		$(this).closest('.tab-dropdown').removeClass('open');
		const hash = $(this).attr("data-href");

		if (hash == "#consult" || hash == "#web-apps") {
			newUrl = url.split("#")[0];
		} else {
			newUrl = url.split("#")[0] + hash;
		}
		newUrl += " ";
		history.replaceState(null, null, newUrl);

		$('.tab-pane').removeClass('show active');
		$(".tab-pane[id=" + attr + "]").addClass('show active');
	});


	$('body, html').click(function () {
		$('.tab-dropdown').removeClass('open');
	})
}


function blogScroller () {
	let list = $('.blog-navigation li');
	list.click(function () {
		list.removeClass('active');
		$(this).addClass('active');

		let scrollTo = $(this).attr('data-target');

		$('html, body').animate({
			scrollTop: ($('*[data-scroll="' + scrollTo + '"]').offset().top - 40)
		}, 500);
	});
}


function popup (elem, param) {
	let timeout;
	elem.fadeIn('fast');
	elem.find('.popup').addClass('active').fadeIn();
	if (param) {
		 timeout = setTimeout(function () {
			elem.fadeOut('fast');
			elem.find('.popup').removeClass('active').fadeOut();
		}, param);
	}
	elem.fadeIn('fast');
	elem.find('.popup').addClass('active').fadeIn();
	elem.click(function () {
		$(this).fadeOut();
		$(this).find('.popup').removeClass('active').fadeOut('fast');
		clearTimeout(timeout);
	});
	elem.find('.popup').click(function (e) {
		e.stopPropagation();
	});
}

function dragNDrop(){
	let fileInput = $('.file-input');
	let droparea = $('.file-drop-area');

// highlight drag area
	fileInput.on('dragenter focus click', function() {
		droparea.addClass('is-active');
	});

// back to normal state
	fileInput.on('dragleave blur drop', function() {
		droparea.removeClass('is-active');
	});

// change inner text
	fileInput.on('change', function() {
		let filesCount = $(this)[0].files.length;
		let $textContainer = $(this).prev('.js-set-number');

		if (filesCount === 1) {
			// if single file then show file name
			$textContainer.text($(this).val().split('\\').pop());
		} else {
			// otherwise show number of files
			$textContainer.text(filesCount + ' files selected');
		}
	});
}
//career form sent popup
// popup($('.career-success-popup'), 6000);

//runs on careers page
$('.apply-now-btn').click(function (e) {
	e.preventDefault();
	popup($('.career-form-popup'));
});


$(function(){
	hamburger();
	setCarousels();
	tabLocation();
	footerDropdown();
	accordeon();
	getQuoteForm();
	textareaLimit();
	tabDropdown();
	blogScroller();
	dragNDrop();
});

//# sourceMappingURL=main.js.map
