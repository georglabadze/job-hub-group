$(() => {
    $("form#get-quote-form").submit(async (event) => {
        event.preventDefault();
        let lead = {};
        let inputsArr = $("form#get-quote-form").serializeArray();
        for (let obj of inputsArr) {
            lead[obj.name] = obj.value;
        }
        await fetch('https://o5y45h4b98.execute-api.us-east-1.amazonaws.com/prod/quotesApi', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(lead)
        });
        popup($('.email-sent-popup'), 1000);
        setTimeout(() => {
            window.location = window.location.origin + '/get-a-quote-success.html'
        }, 1000)
    });

    $("form#lets-talk-form").submit(async (event) => {
        event.preventDefault();
        let lead = {};
        let inputsArr = $("form#lets-talk-form").serializeArray();
        for (let obj of inputsArr) {
            lead[obj.name] = obj.value;
        }
        await fetch('https://o5y45h4b98.execute-api.us-east-1.amazonaws.com/prod/contactUsApi', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(lead)
        });
        popup($('.email-sent-popup'), 1000);
    });

    $("form#investor-newsletter-form").submit(async (event) => {
        event.preventDefault();
        let lead = {};
        let inputsArr = $("form#investor-newsletter-form").serializeArray();
        for (let obj of inputsArr) {
            lead[obj.name] = obj.value;
        }
        await fetch('https://o5y45h4b98.execute-api.us-east-1.amazonaws.com/prod/investorNewsletterApi', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(lead)
        });
        popup($('.email-sent-popup'), 1000);
    })
});

//# sourceMappingURL=forms.js.map
